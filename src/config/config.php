<?php

//gen
$rep=__DIR__.'/../';

// Nombre de news à afficher par page
$limit = 5;

require_once("Connection.php");

// DB
$dsn = 'mysql:host=localhost;dbname=dbName'; // The Data Source Name
$user = ''; // User of the database
$password= ''; // Password for the user of the database

// Error views
$vues['error']='view/errors/error.php';
$vues['error-401']='view/errors/error-401.php';
$vues['error-404']='view/errors/error-404.php';
$vues['error-405']='view/errors/error-405.php';
$vues['error-418']='view/errors/error-418.php';

// Views
$vues['blog']='view/blog.php';
$vues['news']='view/news.php';
$vues['login']='view/login.php';
$vues['admin']='view/admin.php';
$vues['adminNews']='view/adminNews.php';
