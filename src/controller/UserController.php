<?php

class UserController {
	private $nbNews;
	private $nbComment;
	private $nbCommentUser;

	function __construct() {
		global $rep,$vues;
		$dViewError = array();

		$this->nbNews = NewsModel::getNbNews();
		$this->nbComment = CommentModel::getNbComment();
		if (isset($_COOKIE['nbCommentUser'])) {
			$this->nbCommentUser = $_COOKIE['nbCommentUser'];
            Validation::val_cookie_nbCommentUser($this->nbComment, $this->nbCommentUser);
		}
		else {
			$this->nbCommentUser = 0;
		}

		try {
			if (isset($_REQUEST['action'])) {
				$action=$_REQUEST['action'];
			}
			else {
				$action=NULL;
			}

			switch($action) {
				case NULL: //Pas d'action, appel de la page d'accueil du blog avec toutes les news
					$this->Blog();
					break;
				case "news": //Appel de la page affichant une news
					$this->News($dViewError);
					break;
				case "comment": //Vérification, et ajout d'un commentaire
					$this->Comment($dViewError);
					break;
				case "search": //Appel de la page d'accueil du blog, mais avec les résultats de la recherche comme news
					$this->Search($dViewError);
					break;
				case "login": //Appel de la page d'accueil du blog, mais avec les résultats de la recherche comme news
					$this->Login();
					break;
				case "connection": //Appel de la page d'accueil du blog, mais avec les résultats de la recherche comme news
					$this->Connection($dViewError);
					break;
				case "coffee":
					$this->Coffee(); //Easter egg avec l'erreur http 418
					break;
				default: //mauvaise action
					$dViewError[] =	"Erreur d'appel php.";
					require ($rep.$vues['error']);
					break;
			}

		} catch (PDOException $e) {
			$dViewError[] =	"Erreur inattendue avec la base de donnée !";
			require ($rep.$vues['error']);
		}
		catch (Exception $e)
		{
			$dViewError[] =	"Erreur inattendue !";
			require ($rep.$vues['error']);
		}

		exit(0);
	}


	function Blog() {
		global $rep,$vues,$limit;

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        else {
            $page = 1;
        }

        Validation::val_page($page);

		$nbNews = NewsModel::getNbNewsPublished();
		$nbPages = ceil($nbNews / $limit);
		$listNews = NewsModel::getAllNewsPublished($limit, $page);
		require ($rep.$vues['blog']);
	}

	function News(array $dViewError) {
		global $rep,$vues;

		if (isset($_REQUEST['newsID'])) {
			$newsID = $_REQUEST['newsID'];
		}
		else {
			$newsID = '';
		}

		Validation::val_idNews($newsID, $dViewError);
		if (isset($_SESSION['pseudo'])) {
			$pseudo=$_SESSION['pseudo'];
		}
		else {
			$pseudo='';
		}
		$comment='';


		if (count($dViewError) != 0) {
			require ($rep.$vues['error']);
		}
		else {
			$news = NewsModel::getNewsPublishedById($newsID);
			$comments = CommentModel::getAllCommentsByNewsID($newsID);
			if (isset($news)) {
				require ($rep.$vues['news']);
			}
			else {
				require ($rep.$vues['error-404']);
			}
		}
	}

	function comment(array $dViewError) {
		global $rep,$vues;

		if (isset($_POST['pseudo'])) {
			$pseudo=$_POST['pseudo'];
			$_SESSION['pseudo'] = $_POST['pseudo'];
		}
		else {
			$pseudo='';
		}
		if (isset($_POST['comment'])) {
			$comment=$_POST['comment'];
		}
		else {
			$comment='';
		}
		if (isset($_POST['newsID'])) {
			$newsID=$_POST['newsID'];
		}
		else {
			$newsID='';
		}

		Validation::val_comment($pseudo,$comment, $newsID,$dViewError);

		if ($newsID == -1) {
			require ($rep.$vues['error']);
		}
		else {

			if (count($dViewError) == 0) {
                if (isset($_COOKIE['nbCommentUser'])) {
                    $this->nbCommentUser = $_COOKIE['nbCommentUser'];
                    Validation::val_cookie_nbCommentUser($this->nbComment, $this->nbCommentUser);
                    setcookie('nbCommentUser', $this->nbCommentUser+1, time() + 365 * 24 * 3600 * 2, null, null, false, true);
                }
                else {
                    setcookie('nbCommentUser', 1, time() + 365 * 24 * 3600 * 2, null, null, false, true);
                }
				CommentModel::insertComment($pseudo, $comment, $newsID);
				header('Location: ?action=news&newsID=' . $newsID);
			}
			else {
				$news = NewsModel::getNewsPublishedById($newsID);
				$comments = CommentModel::getAllCommentsByNewsID($newsID);
				require ($rep.$vues['news']);
			}
		}
	}

	function Search($dViewError) {
		global $rep,$vues;

		if (isset($_POST['search'])) {
			$keyWord=$_POST['search'];
		}
		else {
			$keyWord='';
		}

		Validation::val_search($keyWord, $dViewError);

		$listNews = NewsModel::getNewsPublishedByTitleContent($keyWord);
		require ($rep.$vues['blog']);
	}

	function Login() {
		global $rep,$vues;

		$admin = AdminModel::isAdmin();

		if ($admin == NULL) {
			require ($rep.$vues['login']);
		}
		else {
			$listNews = NewsModel::getAllNews();
			require ($rep.$vues['admin']);
		}
	}

	function Connection($dViewError) {
		global $rep,$vues;

		if (isset($_POST['pseudo'])) {
			$pseudo=$_POST['pseudo'];
		}
		else {
			$pseudo='';
		}

		if (isset($_POST['password'])) {
			$password=$_POST['password'];
		}
		else {
			$password='';
		}

		Validation::val_connection($pseudo, $password, $dViewError);

		if (count($dViewError) == 0) {
			$isAdmin = AdminModel::connection($pseudo, $password);

			switch($isAdmin) {
				case 1:
					header('Location: ?action=admin');
					break;
				case 0:
					$dViewError[] =	"Le pseudo et le mot de passe ne correspondent pas ou l'administrateur n'est pas dans notre base de donnée.";
					require ($rep.$vues['login']);
					break;
				default:
					$dViewError[] = "Erreur avec la base de donnée.";
					require($rep . $vues['error']);
					break;
			}
		}
		else {
			require ($rep.$vues['login']);
		}
	}

	function Coffee() {
		global $rep,$vues;

		require ($rep.$vues['error-418']);
	}
}
