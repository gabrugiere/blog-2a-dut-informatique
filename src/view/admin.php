<!DOCTYPE html>
<html lang="fr">
	<head>
		<?php include 'includes/meta.php'; ?>
		<link rel="stylesheet" href="view/style/admin.css" type="text/css" />

		<title>Admin | ProgWeb</title>
	</head>

	<body>
		<?php
			if (isset($admin))
			{
				if (isset($listNews))
				{
		?>
		<header>
			<?php include 'includes/headerAdmin.php'; ?>
		</header>

		<main>
			<section>
				<?php
					if (isset($dViewError) && count($dViewError)>0)
					{
				?>
				<div class="alert alert-danger" role="alert">
				<?php
					foreach ($dViewError as $value){
						echo $value.'<br/>';
					}
				?>
				</div>
				<?php
					}
				?>
				<ul class="list-group">
					<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-secondary">
						Titre
						<span>
							<a href="?action=add"><button type="button" class="btn btn-success btn-sm">Ajouter une news</button></a>
							<button type="button" class="btn btn-info btn-sm disabled" disabled>Publié</button>
							<button type="button" class="btn btn-warning btn-sm disabled" disabled>Modifier</button>
							<button type="button" class="btn btn-danger btn-sm disabled" disabled>Supprimer</button>
						</<span>
					</li>
					<?php
						foreach ($listNews as $news) {
					?>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						<a href="?action=news&newsID=<?= $news->getId()?>"><?= $news->getTitle() ?></a>
						<span>
							<span>
								<?php
									if ($news->getPublished()) {
										echo '<img class="" src="view/style/icons/square-fill.svg" alt="" width="24" height="24">';
									}
									else {
										echo '<img class="" src="view/style/icons/square.svg" alt="" width="24" height="24">';
									}
								?>
							</span>
							<a href="?action=modify&&newsID=<?= $news->getId() ?>"><button type="button" class="btn btn-warning btn-sm">Modifier</button></a>
							<a href="?action=delete&&newsID=<?= $news->getId() ?>"><button type="button" class="btn btn-danger btn-sm">Supprimer</button></a>
						</span>
					</li>
					<?php
						}
					?>
				</ul>
			</section>
			<?php
				if (isset($page))
				{
			?>
			<section>
				<nav>
					<ul class="pagination justify-content-center">
						<li class="page-item <?php if ($page == 1) echo "disabled"?>">
							<a class="page-link" href="?action=admin&page=<?= $page - 1; ?>" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<?php
							for ($i = 1; $i <= $nbPages; $i++)
							{
						?>

						<li class="page-item <?php if ($page == $i) echo "active";?>"><a class="page-link" href="?action=admin&page=<?= $i; ?>"><?= $i; ?></a></li>

						<?php
							}
						?>
						<li class="page-item <?php if ($page == $nbPages) echo "disabled"?>">
							<a class="page-link" href="?action=admin&page=<?= $page + 1; ?>" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
			</section>
			<?php
				}
			?>
		</main>

		<footer class="mt-auto">
			<?php include 'includes/footer.php'; ?>
		</footer>

		<?php
				}
				else {
					require 'errors/error-405.php';
				}
			}
			else {
				require 'errors/error-401.php';
			}
		?>

		<script src="bootstrap/js/jquery.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
	</body>
</html>
