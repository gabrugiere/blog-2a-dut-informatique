<?php

	// Chargement de la config
	require_once(__DIR__.'/config/config.php');

	// Chargement de l'autoloader pour autocharger les classes
	require_once(__DIR__.'/config/Autoload.php');
	Autoload::charger();

	$cont = new FrontController();
