<?php

class NewsGateway {
	private $con;
	private $formatDate = "d/m/Y H\hi";

	public function __construct(Connection $con) {
		$this->con = $con;
	}

	public function findAllNews(int $limit, int $page) : array {
		$beginning = ($page -1) * $limit;

		$query = "SELECT * FROM b_news LIMIT :limit OFFSET :beginning";

		$this->con->executeQuery($query, array(
			':limit' => array($limit, PDO::PARAM_INT),
			':beginning' => array($beginning, PDO::PARAM_INT)));

		$results = $this->con->getResults();
		$listNews = array();
		Foreach ($results as $news)
			$listNews[]=new News($news['id'], $news['title'], $news['image'], date($this->formatDate, strtotime($news['date'])), $news['content'], $news['published']);
		return $listNews;
	}

	public function findAllNewsPublished(int $limit, int $page) {
	    $beginning = ($page -1) * $limit;

		$query = "SELECT * FROM b_news WHERE published=:published LIMIT :limit OFFSET :beginning";

		$this->con->executeQuery($query, array(
			':published' => array(1, PDO::PARAM_BOOL),
			':limit' => array($limit, PDO::PARAM_INT),
			':beginning' => array($beginning, PDO::PARAM_INT)
		));

		$results = $this->con->getResults();
		$listNews = array();
		Foreach ($results as $news)
			$listNews[]=new News($news['id'], $news['title'], $news['image'], date($this->formatDate, strtotime($news['date'])), $news['content'], $news['published']);
		return $listNews;
	}

	public function findNewsById(int $ID) : News {
		$query = "SELECT * FROM b_news WHERE id=:id";

		$this->con->executeQuery($query, array(
			':id' => array($ID, PDO::PARAM_INT)
		));

		$results = $this->con->getResults();
		foreach ($results as $news)
			return new News($news['id'], $news['title'], $news['image'], date($this->formatDate, strtotime($news['date'])), $news['content'], $news['published']);
	}

	public function findNewsPublishedById(int $ID) {
		$query = "SELECT * FROM b_news WHERE published=:published AND id=:id";

		$this->con->executeQuery($query, array(
			':published' => array(1, PDO::PARAM_BOOL),
			':id' => array($ID, PDO::PARAM_INT)
		));

		$results = $this->con->getResults();
		foreach ($results as $news)
			return new News($news['id'], $news['title'], $news['image'], date($this->formatDate, strtotime($news['date'])), $news['content'], $news['published']);
	}

	function findNewsByTitleContent(string $keyWord) : array {
		$query = "SELECT * FROM b_news WHERE title LIKE :content";

		$this->con->executeQuery($query, array(
			':content' => array('%'.$keyWord.'%', PDO::PARAM_STR)
		));

		$results = $this->con->getResults();
		$listNews=array();
		Foreach ($results as $news)
			$listNews[]=new News($news['id'], $news['title'], $news['image'], date($this->formatDate, strtotime($news['date'])), $news['content'], $news['published']);
		return $listNews;
	}

	function findNewsPublishedByTitleContent(string $keyWord) : array {
		$query = "SELECT * FROM b_news WHERE published=:published AND title LIKE :content";

		$this->con->executeQuery($query, array(
			':published' => array(1, PDO::PARAM_BOOL),
			':content' => array('%'.$keyWord.'%', PDO::PARAM_STR)
		));

		$results = $this->con->getResults();
		$listNews=array();
		Foreach ($results as $news)
			$listNews[]=new News($news['id'], $news['title'], $news['image'], date($this->formatDate, strtotime($news['date'])), $news['content'], $news['published']);
		return $listNews;
	}

	public function findNbNews() : int {
		$query = "SELECT * FROM b_news";

		$this->con->executeQuery($query, array());

		$results = $this->con->getResults();

		return count($results);
	}

	public function findNbNewsPublished() {
		$query = "SELECT * FROM b_news WHERE published=:published";

		$this->con->executeQuery($query, array(
			':published' => array(1, PDO::PARAM_BOOL)
		));

		$results = $this->con->getResults();
		return count($results);
	}

	public function insertNews(string $title, string $image, string $content, bool $published) {
		$query = "INSERT INTO b_news VALUES(NULL, :title, :image, LOCALTIME(), :content, :published)";

		$this->con->executeQuery($query, array(
			':title' => array($title, PDO::PARAM_STR),
			':image' => array($image, PDO::PARAM_STR),
			':content' => array($content, PDO::PARAM_STR),
			':published' => array($published, PDO::PARAM_BOOL)
		));
	}

	public function deleteNews(int $newsID) {
		$query = "DELETE FROM b_news WHERE id=:id";

		$this->con->executeQuery($query, array(
			':id' => array($newsID, PDO::PARAM_INT)
		));

		$query = "DELETE FROM b_comment WHERE newsID=:id";

		$this->con->executeQuery($query, array(
			':id' => array($newsID, PDO::PARAM_INT)
		));
	}

	public function updateNews(int $id, string $title, string $image, string $content, bool $published) {
		$query = "UPDATE b_news SET title=:title, image=:image, content=:content, published=:published WHERE id=:id";

		$this->con->executeQuery($query, array(
			':title' => array($title, PDO::PARAM_STR),
			':image' => array($image, PDO::PARAM_STR),
			':content' => array($content, PDO::PARAM_STR),
			':published' => array($published, PDO::PARAM_BOOL),
			':id' => array($id, PDO::PARAM_INT)
		));
	}
}