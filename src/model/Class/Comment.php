<?php

class Comment {
	protected $id;
	protected $pseudo;
	protected $date;
	protected $content;
	protected $newsID;

	function __construct(int $id, string $pseudo, string $date, string $content, string $newsID) {
		$this->id=$id;
		$this->pseudo=$pseudo;
		$this->date=$date;
		$this->content=$content;
		$this->newsID=$newsID;
	}

	/**
	 * @return int
	 */
	public function getId(): int { return $this->id; }

	/**
	 * @return string
	 */
	public function getPseudo(): string { return $this->pseudo; }

	/**
	 * @return string
	 */
	public function getDate(): string { return $this->date; }

	/**
	 * @return string
	 */
	public function getContent(): string { return $this->content; }

	/**
	 * @return string
	 */
	public function getNewsID(): string { return $this->newsID; }
}